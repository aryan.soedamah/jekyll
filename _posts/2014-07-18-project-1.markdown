---
layout: default
modal-id: 6
img: technical.png
alt: image-alt
category: SQL, Open API, ELK stack, Virtualization etc.
description: Query a database, specify data-contracts with open api 3.0, implement an ELK stack for logging, use virtualization solutions like docker or kubernetes 
---
